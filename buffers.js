import WebGL from './webgl.js';

const Buffers = ((gl) => {
  const getBuffer = (
    data = null,
    bindPoint = gl.ARRAY_BUFFER, // gl.ELEMENT_ARRAY_BUFFER
  ) => {
    const buffer = gl.createBuffer();
    gl.bindBuffer(bindPoint, buffer);
    if (data) gl.bufferData(bindPoint, data, gl.STATIC_DRAW);
    return buffer;
  };

  const getFrameBuffer = (
    bind = true,
  ) => {
    const fb = gl.createFramebuffer();
    if (bind) gl.bindFramebuffer(gl.FRAMEBUFFER, fb);
    return fb;
  };

  const setFramebufferTexture2D = (
    texture,
    target = gl.FRAMEBUFFER,
    attachment = gl.COLOR_ATTACHMENT0,
    textarget = gl.TEXTURE_2D,
    level = 0,
  ) => {
    gl.framebufferTexture2D(target, attachment, textarget, texture, level);
  };

  const setVertexAttribPointer = (
    index,
    size,
    type = gl.FLOAT,
    normalized = false,
    stride = 0,
    offset = 0,
  ) => {
    gl.vertexAttribPointer(index, size, type, normalized, stride, offset);
  };

  return Object.freeze({
    getBuffer,
    getFrameBuffer,
    setFramebufferTexture2D,
    setVertexAttribPointer,
  });
})(WebGL.getGL());

export default Buffers;
