const Config = (() => {
  const cubeFaces = {
    front: 4, back: 5, top: 3, bottom: 2, left: 1, right: 0,
  };
  const imgPath = 'lake256/images/';
  const faceSize = 2048;
  const faceClipSpaceSize = 2; // from -1 to 1
  const faceTexCoordSize = 1;
  const tilesInRow = 1;
  const tilesInFace = tilesInRow ** 2;
  const tileSize = faceSize / tilesInRow;
  const tileClipSpaceSize = faceClipSpaceSize / tilesInRow;
  const tileTexCoordSize = faceTexCoordSize / tilesInRow;
  const canvasSize = window.innerHeight;
  const shaderParameters = {};
  const viewportChange = false;

  return Object.freeze({
    cubeFaces,
    imgPath,
    faceSize,
    faceClipSpaceSize,
    faceTexCoordSize,
    tilesInRow,
    tilesInFace,
    tileSize,
    tileClipSpaceSize,
    tileTexCoordSize,
    canvasSize,
    shaderParameters,
    viewportChange,
  });
})();

export default Config;
