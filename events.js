import WebGL from './webgl.js';

const Events = ((canv) => {
  const canvas = canv;
  let drag = false;
  let oldX;
  let oldY;
  let dX = 0;
  let dY = 0;
  let THETA = 0;
  let PHI = 0;

  const mouseDown = (e) => {
    drag = true;
    oldX = e.pageX;
    oldY = e.pageY;
    e.preventDefault();
    return false;
  };

  const mouseUp = () => {
    drag = false;
  };

  const mouseMove = (e) => {
    if (!drag) return false;
    dX = (e.pageX - oldX) * 2 * Math.PI / canvas.width;
    dY = (e.pageY - oldY) * 2 * Math.PI / canvas.height;
    THETA -= dX;
    PHI -= dY;
    oldX = e.pageX;
    oldY = e.pageY;
    e.preventDefault();
    return false;
  };

  const windowResize = () => {
    canvas.width = window.innerHeight;
    canvas.height = window.innerHeight;
  };

  const addAllListeners = () => {
    canvas.addEventListener('mousedown', mouseDown, false);
    canvas.addEventListener('mouseup', mouseUp, false);
    canvas.addEventListener('mouseout', mouseUp, false);
    canvas.addEventListener('mousemove', mouseMove, false);
    window.addEventListener('resize', windowResize, false);
  };

  const getTheta = () => THETA;
  const getPhi = () => PHI;
  const getDrag = () => drag;

  return Object.freeze({
    addAllListeners,
    getTheta,
    getPhi,
    getDrag,
  });
})(WebGL.getCanvas());

export default Events;
