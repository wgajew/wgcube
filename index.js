import Config from './config.js';
import Load from './loading.js';
import WebGL from './webgl.js';
import Shaders from './shaders.js';
import Programs from './programs.js';
import Geometry from './geometry.js';
import Textures from './textures.js';
import Buffers from './buffers.js';
import tileComposer from './tilecomposer.js';
import Utils from './utils.js';
import Events from './events.js';

function main(gl, canv, canvasSize, tileSize, cubeFaces, tilesInFace) {
  const canvas = canv;
  canvas.width = canvasSize;
  canvas.height = canvasSize;
  const fieldOfViewRadians = Utils.degToRad(60);

  console.time('main');

  Events.addAllListeners();

  // shader programs
  const program1 = Programs.getProgram(Shaders.getVertexShader(), Shaders.getFragmentShader());
  const program2 = Programs.getProgram(Shaders.getVertexShader2(), Shaders.getFragmentShader2());
  const program3 = Programs.getProgram(Shaders.getVertexShader(), Shaders.getFragmentShader3());

  // attributes
  const vertexLocation = gl.getAttribLocation(program1, 'a_position');
  const texcoordLocation = gl.getAttribLocation(program1, 'a_texCoord');
  const vertexLocation2 = gl.getAttribLocation(program2, 'a_position');

  // uniforms
  const textureLocation = gl.getUniformLocation(program1, 'u_image');
  const flipYLocation = gl.getUniformLocation(program1, 'u_flipY');
  const modeLocation = gl.getUniformLocation(program1, 'u_mode');
  const texture1Location = gl.getUniformLocation(program3, 'texture1');
  const texture2Location = gl.getUniformLocation(program3, 'texture2');
  const texture3Location = gl.getUniformLocation(program3, 'texture3');
  const texture4Location = gl.getUniformLocation(program3, 'texture4');
  const projectionMatrixLocation = gl.getUniformLocation(program2, 'u_Pmatrix');
  const viewMatrixLocation = gl.getUniformLocation(program2, 'u_Vmatrix');
  const cubeTextureLocation = gl.getUniformLocation(program2, 'u_cube_texture');

  // buffers
  const vertexBuffer = Buffers.getBuffer(Geometry.vertices);
  const indexBuffer = Buffers.getBuffer(Geometry.indices, gl.ELEMENT_ARRAY_BUFFER);
  const texcoordBuffer = Buffers.getBuffer(Geometry.coordinates);
  const vertexBuffer3d = Buffers.getBuffer(Geometry.vertices3d);
  const indexBuffer3d = Buffers.getBuffer(Geometry.indices3d, gl.ELEMENT_ARRAY_BUFFER);

  // Create textures.
  // Textures.prepareAllTextures(Load.frontImages, Load.backImages, Load.topImages, Load.bottomImages, Load.leftImages, Load.rightImages);
  Textures.prepareAllTextures(null, null, null, null, null, null);

  const cubeTexture = Textures.createAndNullifyTexture3D();

  // Create and bind the framebuffer
  const frameBuffer = Buffers.getFrameBuffer();

  const programData = {
    vertexBuffer,
    indexBuffer,
    texcoordBuffer,
    vertexLocation,
    texcoordLocation,
    textureLocation,
    flipYLocation,
    modeLocation,
    frameBuffer,
    cubeTexture,
    texture1Location,
    texture2Location,
    texture3Location,
    texture4Location,
  };

  const blankTiles = [
    { faceId: 0, tileId: 1 },
    { faceId: 0, tileId: 2 },
    { faceId: 5, tileId: 1 },
    { faceId: 5, tileId: 3 },
    { faceId: 5, tileId: 4 },
    { faceId: 3, tileId: 1 },
    { faceId: 3, tileId: 2 },
    { faceId: 3, tileId: 4 },
    { faceId: 4, tileId: 1 },
    { faceId: 2, tileId: 1 },
    { faceId: 1, tileId: 1 },
  ];

  const moonImg = Utils.getCanvas2d(Load.otherImages[0].image, tileSize, tileSize);
  const redImg = Load.otherImages[1].image;
  const red2Img = Load.otherImages[2].image;
  const greenImg = Load.otherImages[3].image;
  const canvasImg = Utils.getCanvas2d(Load.otherImages[4].image, tileSize, tileSize);
  const smallTextures = {
    moon: Textures.createAndSetupTexture2D(moonImg),
    red: Textures.createAndSetupTexture2D(redImg),
    red2: Textures.createAndSetupTexture2D(red2Img),
    green: Textures.createAndSetupTexture2D(greenImg),
    canvas: Textures.createAndSetupTexture2D(canvasImg),
  };

  const frontImg = Load.bigFrontImages[0].image;
  const backImg = Load.bigBackImages[0].image;
  const topImg = Load.bigTopImages[0].image;
  const bottomImg = Load.bigBottomImages[0].image;
  const leftImg = Load.bigLeftImages[0].image;
  const rightImg = Load.bigRightImages[0].image;
  const bigTextures = {
    front: Textures.createAndSetupTexture2D(frontImg),
    back: Textures.createAndSetupTexture2D(backImg),
    top: Textures.createAndSetupTexture2D(topImg),
    bottom: Textures.createAndSetupTexture2D(bottomImg),
    left: Textures.createAndSetupTexture2D(leftImg),
    right: Textures.createAndSetupTexture2D(rightImg),
  };

  function drawOnTiles() {
    const filteredTiles = Textures.getFilteredTiles(blankTiles);
    for (let i = 0, l = filteredTiles.length; i < l; i++) {
      const face = Object.keys(cubeFaces).find(key => cubeFaces[key] === filteredTiles[i].faceId);
      const texturesToBlend = [{ texture: bigTextures[face], add: true, mode: 1 }];
      filteredTiles[i].texture = Textures.getBlended(program1, programData, texturesToBlend, filteredTiles[i].tileId);
      tileComposer.drawOnTile(filteredTiles[i], program1, programData);
      console.log(`tile size: ${tileSize}`);
    }
  }

  function drawOnBlankTiles(clear = false) {
    if (clear) {
      for (const { faceId, tileId } of blankTiles) {
        const tile = Textures.getTileWithMultipleTextures(faceId, tileId, []);
        tileComposer.drawOnBlankTile(tile, program3, programData, clear);
      }
    } else {
      for (const { faceId, tileId } of blankTiles) {
        const face = Object.keys(cubeFaces).find(key => cubeFaces[key] === faceId);
        const texture0 = [{ texture: bigTextures[face], add: true, mode: 0 }];
        const texture1 = Textures.getBlended(program1, programData, texture0, tileId);
        const textures = [texture1, smallTextures.red, smallTextures.red2, smallTextures.green];
        const tile = Textures.getTileWithMultipleTextures(faceId, tileId, textures);
        tileComposer.drawOnBlankTile(tile, program3, programData, clear);
      }
    }
  }

  drawOnTiles();
  drawOnBlankTiles();

  const chbx = document.getElementById('check');
  chbx.onchange = function () {
    if (this.checked) {
      drawOnBlankTiles();
    } else {
      drawOnBlankTiles(true);
    }
    drawScene(0);
  };

  // Draw the scene.
  function drawScene(mode = 1) {
    if (Events.getDrag() || mode === 0) {
      gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

      // Tell it to use our program (pair of shaders)
      gl.useProgram(program2);

      gl.bindFramebuffer(gl.FRAMEBUFFER, null);

      // Bind the position buffer.
      gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer3d);

      // Tell the position attribute how to get data out of vertexBuffer (ARRAY_BUFFER)
      const size = 3; // 3 components per iteration
      Buffers.setVertexAttribPointer(vertexLocation2, size);
      gl.enableVertexAttribArray(vertexLocation2);

      // Compute the projection matrix
      const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
      const { mat4 } = glMatrix;
      const projectionMatrix = mat4.perspective(mat4.create(), fieldOfViewRadians, aspect, 1, 2000);

      const cameraPosition = [0, 0, 1];
      const up = [0, 1, 0];
      const target = [0, 0, 0];

      // Compute the camera's matrix using look at.
      const viewMatrix = mat4.lookAt(mat4.create(), cameraPosition, target, up);

      mat4.rotateX(viewMatrix, viewMatrix, Events.getPhi());
      mat4.rotateY(viewMatrix, viewMatrix, Events.getTheta());

      // Set the matrix.
      gl.uniformMatrix4fv(projectionMatrixLocation, false, projectionMatrix);
      gl.uniformMatrix4fv(viewMatrixLocation, false, viewMatrix);

      // Tell the shader to use texture unit 0 for u_cube_texture
      gl.uniform1i(cubeTextureLocation, 0);
      gl.activeTexture(gl.TEXTURE0);

      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer3d);
      gl.drawElements(gl.TRIANGLES, Geometry.indices3d.length, gl.UNSIGNED_SHORT, 0);
    }
    requestAnimationFrame(drawScene);
  }

  drawScene(0);
  console.timeEnd('main');
}

function init() {
  Load.loadAllImages(() => {
    main(WebGL.getGL(), WebGL.getCanvas(), Config.canvasSize, Config.tileSize, Config.cubeFaces, Config.tilesInFace);
  });
}
init();
