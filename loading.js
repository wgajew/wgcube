import Config from './config.js';

const Load = ((cubeFaces, imagesPerFace, imgPath) => {
  const frontImages = [];
  const backImages = [];
  const topImages = [];
  const bottomImages = [];
  const leftImages = [];
  const rightImages = [];
  const bigFrontImages = [];
  const bigBackImages = [];
  const bigTopImages = [];
  const bigBottomImages = [];
  const bigLeftImages = [];
  const bigRightImages = [];
  const otherImages = [];

  const loadAllImages = (callback) => {
    let pic = 0;
    for (const face of Object.keys(cubeFaces)) {
      let faceImages = imagesPerFace > 64 ? 64 : imagesPerFace;
      for (let i = 1; i <= faceImages; i += 1) {
        ((f, ii) => {
          const j = ii < 10 ? `0${ii}` : ii;
          const im = new Image();
          im.src = `${imgPath}${f}_${j}.jpg`;
          im.onload = () => {
            pic += 1;
            switch (f) {
              case 'front':
                frontImages.push({ image: im, src: im.src });
                break;
              case 'back':
                backImages.push({ image: im, src: im.src });
                break;
              case 'top':
                topImages.push({ image: im, src: im.src });
                break;
              case 'bottom':
                bottomImages.push({ image: im, src: im.src });
                break;
              case 'left':
                leftImages.push({ image: im, src: im.src });
                break;
              case 'right':
                rightImages.push({ image: im, src: im.src });
                break;
              default:
                break;
            }
            if (pic === faceImages * Object.keys(cubeFaces).length) {
              frontImages.sort((a, b) => a.src.localeCompare(b.src));
              backImages.sort((a, b) => a.src.localeCompare(b.src));
              topImages.sort((a, b) => a.src.localeCompare(b.src));
              bottomImages.sort((a, b) => a.src.localeCompare(b.src));
              leftImages.sort((a, b) => a.src.localeCompare(b.src));
              rightImages.sort((a, b) => a.src.localeCompare(b.src));
              const otherImg = new Image();
              otherImg.src = 'moon.jpg';
              otherImg.onload = () => {
                otherImages.push({ image: otherImg, src: otherImg.src });
                const redImg = new Image();
                redImg.src = 'red1.png';
                redImg.onload = () => {
                  otherImages.push({ image: redImg, src: redImg.src });
                  const redImg2 = new Image();
                  redImg2.src = 'red2.png';
                  redImg2.onload = () => {
                    otherImages.push({ image: redImg2, src: redImg2.src });
                    const greenImg = new Image();
                    greenImg.src = 'green1.png';
                    greenImg.onload = () => {
                      otherImages.push({ image: greenImg, src: greenImg.src });
                      const canvasImg = new Image();
                      canvasImg.src = '0_100_0.png';
                      canvasImg.onload = () => {
                        otherImages.push({ image: canvasImg, src: canvasImg.src });
                        const bigFrontImg = new Image();
                        bigFrontImg.src = 'lake/front.jpg';
                        bigFrontImg.onload = () => {
                          bigFrontImages.push({ image: bigFrontImg, src: bigFrontImg.src });
                          const bigBackImg = new Image();
                          bigBackImg.src = 'lake/back.jpg';
                          bigBackImg.onload = () => {
                            bigBackImages.push({ image: bigBackImg, src: bigBackImg.src });
                            const bigTopImg = new Image();
                            bigTopImg.src = 'lake/top.jpg';
                            bigTopImg.onload = () => {
                              bigTopImages.push({ image: bigTopImg, src: bigTopImg.src });
                              const bigBottomImg = new Image();
                              bigBottomImg.src = 'lake/bottom.jpg';
                              bigBottomImg.onload = () => {
                                bigBottomImages.push({ image: bigBottomImg, src: bigBottomImg.src });
                                const bigLeftImg = new Image();
                                bigLeftImg.src = 'lake/left.jpg';
                                bigLeftImg.onload = () => {
                                  bigLeftImages.push({ image: bigLeftImg, src: bigLeftImg.src });
                                  const bigRightImg = new Image();
                                  bigRightImg.src = 'lake/right.jpg';
                                  bigRightImg.onload = () => {
                                    bigRightImages.push({ image: bigRightImg, src: bigRightImg.src });
                                    callback();
                                  };
                                };
                              };
                            };
                          };
                        };
                      };
                    };
                  };
                };
              };
            }
          };
        })(face, i);
      }
    }
  };

  return Object.freeze({
    frontImages,
    backImages,
    topImages,
    bottomImages,
    leftImages,
    rightImages,
    bigFrontImages,
    bigBackImages,
    bigTopImages,
    bigBottomImages,
    bigLeftImages,
    bigRightImages,
    otherImages,
    loadAllImages,
  });
})(Config.cubeFaces, Config.tilesInFace, Config.imgPath);

export default Load;
