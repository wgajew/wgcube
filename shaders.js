import Config from './config.js';

const Shaders = ((Params) => {
  let Parameters = Params;

  const getVertexShader = () => `
    attribute vec2 a_position;
    attribute vec2 a_texCoord;
    varying vec2 v_texCoord;
    
    void main() {
      gl_Position = vec4(a_position, 0, 1);
      v_texCoord = a_texCoord;
    }
  `;

  const getFragmentShader = () => `
    precision mediump float;
    // our textures
    uniform sampler2D u_image;
    // the texCoords passed in from the vertex shader.
    varying vec2 v_texCoord;
    
    uniform float u_flipY;
    uniform int u_mode;
    
    void main() {
        vec4 color = texture2D(u_image, vec2(v_texCoord.x, v_texCoord.y * u_flipY));
        if (u_mode == 1) {
          if (color.r > 0.0 && color.r < 0.1) {
            color.r = color.r + 0.5;
            if (color.r < 0.05) {
              color.r = color.r * v_texCoord.x/v_texCoord.y;
            }
          }
          if (color.r > 0.1 && color.r < 0.2) {
            color.r = color.r + 0.4;
          }
          if (color.r > 0.2 && color.r < 0.3) {
            color.r = color.r + 0.3;
          }
          if (color.r > 0.3 && color.r < 0.4) {
            color.r = color.r + 0.2;
          }
          if (color.r > 0.4 && color.r < 0.5) {
            color.r = color.r + 0.1;
          }
          if (color.r > 0.5 && color.r < 0.6) {
            color.r = color.r - 0.1;
          }
          if (color.r > 0.6 && color.r < 0.7) {
            color.r = color.r - 0.2;
            if (color.r < 0.65) {
              color.r = color.r * v_texCoord.x/v_texCoord.y;
            }
          }
          if (color.r > 0.7 && color.r < 0.8) {
            color.r = color.r - 0.3;
          }
          if (color.r > 0.8 && color.r < 0.9) {
            color.r = color.r - 0.4;
          }
          if (color.r > 0.9) {
            color.r = color.r - 0.5;
          }
          if (color.g > 0.0 && color.g < 0.1) {
            color.g = color.g + 0.5;
          }
          if (color.g > 0.1 && color.g < 0.2) {
            color.g = color.g + 0.4;
          }
          if (color.g > 0.2 && color.g < 0.3) {
            color.g = color.g + 0.3;
            if (color.r < 0.25) {
              color.g = color.g * v_texCoord.x/v_texCoord.y;
            }
          }
          if (color.g > 0.3 && color.g < 0.4) {
            color.g = color.g + 0.2;
          }
          if (color.g > 0.4 && color.g < 0.5) {
            color.g = color.g + 0.1;
          }
          if (color.g > 0.6 && color.g < 0.7) {
            color.g = color.g - 0.2;
          }
          if (color.g > 0.7 && color.g < 0.8) {
            color.g = color.g - 0.3;
            
          }
          if (color.g > 0.8 && color.g < 0.9) {
            color.g = color.g - 0.4;
          }
          if (color.g > 0.9) {
            color.g = color.g - 0.5;
            if (color.g > 0.95) {
              color.g = color.g * v_texCoord.x/v_texCoord.y;
            }
          }
          if (color.b > 0.0 && color.b < 0.1) {
            color.b = color.b + 0.5;
          }
          if (color.b > 0.1 && color.b < 0.2) {
            color.b = color.b + 0.4;
          }
          if (color.b > 0.2 && color.b < 0.3) {
            color.b = color.b + 0.3;
          }
          if (color.b > 0.4 && color.b < 0.5) {
            color.b = color.b + 0.1;
          }
          if (color.b > 0.5 && color.b < 0.6) {
            color.b = color.b - 0.1;
          }
          if (color.b > 0.6 && color.b < 0.7) {
            color.b = color.b - 0.2;
            if (color.b < 0.65) {
              color.b = color.b * v_texCoord.x/v_texCoord.y;
            }
          }
          if (color.b > 0.7 && color.b < 0.8) {
            color.b = color.b - 0.3;
          }
          if (color.b > 0.8 && color.b < 0.9) {
            color.b = color.b - 0.4;
          }
          if (color.b > 0.9) {
            color.b = color.b - 0.5;
          }
        }
        gl_FragColor = color; 
    }
  `;

  const getVertexShader2 = () => `
    attribute vec4 a_position;
    uniform mat4 u_Pmatrix;
    uniform mat4 u_Vmatrix;
    varying vec3 v_normal;
    
    void main() {
      // Multiply the position by the matrix.
      gl_Position = u_Pmatrix * u_Vmatrix * a_position;
      // Pass a normal. Since the positions
      // centered around the origin we can just
      // pass the position
      v_normal = normalize(a_position.xyz);
    }
  `;

  const getFragmentShader2 = () => `
    precision mediump float;
    // Passed in from the vertex shader.
    varying vec3 v_normal;
    // The texture.
    uniform samplerCube u_cube_texture;
    
    void main() {
      vec4 landscape = textureCube(u_cube_texture, normalize(vec3(-v_normal.x,-v_normal.y,v_normal.z)));
      gl_FragColor = landscape;
    }
  `;

  const getFragmentShader3 = () => `
    precision mediump float;
    // our textures
    uniform sampler2D texture1;
    uniform sampler2D texture2;
    uniform sampler2D texture3;
    uniform sampler2D texture4;
    // the texCoords passed in from the vertex shader.
    varying vec2 v_texCoord;
    
    void main() {
      vec4 image1 = texture2D(texture1, v_texCoord);
      vec4 image2 = texture2D(texture2, v_texCoord);
      vec4 image3 = texture2D(texture3, v_texCoord);
      vec4 image4 = texture2D(texture4, v_texCoord);
      gl_FragColor = image1 + image2 + image3 + image4;
    }
  `;

  const getParameters = () => Parameters;
  const updateParameters = (KeyValue) => {
    Parameters = { ...Parameters, ...KeyValue };
  };

  return Object.freeze({
    getVertexShader,
    getFragmentShader,
    getVertexShader2,
    getFragmentShader2,
    getFragmentShader3,
    getParameters,
    updateParameters,
  });
})(Config.shaderParameters);

export default Shaders;
