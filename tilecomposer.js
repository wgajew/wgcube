import WebGL from './webgl.js';
import Buffers from './buffers.js';
import Config from './config.js';
import Geometry from './geometry.js';
import Utils from './utils.js';

const tileComposer = ((gl, faceSize, tileSize, vertices, coordinates) => {
  const drawOnTile = (tile, program, programData) => {
    if (Object.prototype.hasOwnProperty.call(tile, 'destination')) {
      gl.viewport(0, 0, faceSize, faceSize);
    } else {
      gl.viewport(tile.x, tile.y, tileSize, tileSize);
    }

    // Tell it to use our program (pair of shaders)
    gl.useProgram(program);

    gl.bindFramebuffer(gl.FRAMEBUFFER, programData.frameBuffer);

    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_CUBE_MAP_POSITIVE_X + tile.faceId, programData.cubeTexture, 0);
    gl.bindTexture(gl.TEXTURE_2D, tile.texture);

    // Bind the position buffer.
    gl.bindBuffer(gl.ARRAY_BUFFER, programData.texcoordBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, coordinates, gl.STATIC_DRAW);

    // Tell the texCoord attribute how to get data out of texcoordBuffer (ARRAY_BUFFER)
    let size = 2; // 2 components per iteration
    Buffers.setVertexAttribPointer(programData.texcoordLocation, size);
    gl.enableVertexAttribArray(programData.texcoordLocation);

    gl.uniform1i(programData.textureLocation, 0);
    gl.uniform1f(programData.flipYLocation, 1);

    // Bind the position buffer.
    gl.bindBuffer(gl.ARRAY_BUFFER, programData.vertexBuffer);
    if (Object.prototype.hasOwnProperty.call(tile, 'destination')) {
      gl.bufferData(gl.ARRAY_BUFFER, tile.destination, gl.STATIC_DRAW);
    }

    // Tell the position attribute how to get data out of vertexBuffer (ARRAY_BUFFER)
    size = 2; // 2 components per iteration
    Buffers.setVertexAttribPointer(programData.vertexLocation, size);
    gl.enableVertexAttribArray(programData.vertexLocation);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, programData.indexBuffer);
    gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);
  };

  const drawOnBlankTile = (tile, program, programData, clear = false) => {
    if (Object.prototype.hasOwnProperty.call(tile, 'destination')) {
      gl.viewport(0, 0, faceSize, faceSize);
    } else {
      gl.viewport(tile.x, tile.y, tileSize, tileSize);
    }

    gl.useProgram(program);

    gl.bindFramebuffer(gl.FRAMEBUFFER, programData.frameBuffer);
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_CUBE_MAP_POSITIVE_X + tile.faceId, programData.cubeTexture, 0);

    if (clear) {
      gl.enable(gl.BLEND);
      gl.blendFunc(gl.ZERO, gl.ZERO);
    } else {
      gl.activeTexture(gl.TEXTURE0 + 0);
      gl.bindTexture(gl.TEXTURE_2D, tile.textures[0]);
      gl.activeTexture(gl.TEXTURE0 + 1);
      gl.bindTexture(gl.TEXTURE_2D, tile.textures[1]);
      gl.activeTexture(gl.TEXTURE0 + 2);
      gl.bindTexture(gl.TEXTURE_2D, tile.textures[2]);
      gl.activeTexture(gl.TEXTURE0 + 3);
      gl.bindTexture(gl.TEXTURE_2D, tile.textures[3]);
      gl.uniform1i(programData.texture1Location, 0);
      gl.uniform1i(programData.texture2Location, 1);
      gl.uniform1i(programData.texture3Location, 2);
      gl.uniform1i(programData.texture4Location, 3);

      gl.bindBuffer(gl.ARRAY_BUFFER, programData.texcoordBuffer);
      gl.bufferData(gl.ARRAY_BUFFER, coordinates, gl.STATIC_DRAW);
      Buffers.setVertexAttribPointer(programData.texcoordLocation, 2);
      gl.enableVertexAttribArray(programData.texcoordLocation);
    }

    gl.bindBuffer(gl.ARRAY_BUFFER, programData.vertexBuffer);
    if (Object.prototype.hasOwnProperty.call(tile, 'destination')) {
      gl.bufferData(gl.ARRAY_BUFFER, tile.destination, gl.STATIC_DRAW);
    } else {
      gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW);
    }
    Buffers.setVertexAttribPointer(programData.vertexLocation, 2);
    gl.enableVertexAttribArray(programData.vertexLocation);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, programData.indexBuffer);
    gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);

    gl.disable(gl.BLEND);
    gl.activeTexture(gl.TEXTURE0); // bez tego textura 0 (krajobraz) dla kolejnych kafelków jest powiększona
  };

  const drawAndBlend = (program, programData, add, source, mode) => {
    gl.viewport(0, 0, tileSize, tileSize);

    gl.enable(gl.BLEND);
    if (add) {
      gl.blendFunc(gl.ONE, gl.ONE);
      gl.blendEquation(gl.FUNC_ADD);
    } else {
      gl.blendFuncSeparate(gl.ONE, gl.ONE, gl.ONE, gl.ZERO);
      gl.blendEquationSeparate(gl.FUNC_REVERSE_SUBTRACT, gl.FUNC_ADD);
    }

    gl.useProgram(program);

    gl.bindBuffer(gl.ARRAY_BUFFER, programData.texcoordBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, source, gl.STATIC_DRAW);

    Buffers.setVertexAttribPointer(programData.texcoordLocation, 2);
    gl.enableVertexAttribArray(programData.texcoordLocation);

    gl.uniform1i(programData.textureLocation, 0);
    gl.uniform1f(programData.flipYLocation, -1);
    gl.uniform1i(programData.modeLocation, mode);

    gl.bindBuffer(gl.ARRAY_BUFFER, programData.vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW); // vertices reset

    Buffers.setVertexAttribPointer(programData.vertexLocation, 2);
    gl.enableVertexAttribArray(programData.vertexLocation);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, programData.indexBuffer);
    gl.drawElements(gl.TRIANGLES, 6, gl.UNSIGNED_SHORT, 0);

    gl.disable(gl.BLEND);
  };

  return Object.freeze({
    drawOnTile,
    drawOnBlankTile,
    drawAndBlend,
  });
})(WebGL.getGL(), Config.faceSize, Config.tileSize, Geometry.vertices, Geometry.coordinates);

export default tileComposer;
