const WebGL = ((id) => {
  let canvasId = id;
  let gl = null;
  let canvas = null;
  const updateContext = () => {
    if (!canvas || !gl) {
      canvas = document.getElementById(canvasId);
      gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');
      if (gl === null) {
        alert('Unable to initialize WebGL. Your browser or machine may not support it.');
      }
    }
  };
  const getGL = () => {
    updateContext();
    return gl;
  };
  const getCanvas = () => {
    updateContext();
    return canvas;
  };
  const setCanvasId = (cid) => {
    canvasId = cid;
    canvas = null;
  };
  const getCanvasId = () => canvasId;
  return Object.freeze({
    getGL,
    getCanvas,
    getCanvasId,
    setCanvasId,
  });
})('canvas');

export default WebGL;
